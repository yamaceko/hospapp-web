@extends('apps.layout')

@section('title')
    {{ $etablissement->name }} | Fiche du utilisateur
@endsection
@section('breadcrumb-title')
    {{ $etablissement->name }}
@endsection

@section('aside-menu')
    @include('developer/_menu')
@endsection
@section('custom-style')
    <style>
        .btn-circle.btn-xl {
            width: 70px;
            height: 70px;
            padding: 10px 16px;
            border-radius: 35px;
            font-size: 24px;
            line-height: 1.33;
        }

        .btn-circle {
            width: 30px;
            height: 30px;
            padding: 6px 0px;
            border-radius: 15px;
            text-align: center;
            font-size: 12px;
            line-height: 1.42857;
        }

    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            {{-- Gestion des erreurs --}}
            <div class="col-lg-12">
                @if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        @if ($errors->has('message'))
                            <div class="help-block">
                                <strong>{{ $errors->first('message') }}</strong>
                            </div>
                        @endif



                        @if ($errors->has('name'))
                            <div class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </div>
                        @endif

                        @if ($errors->has('adresse'))
                            <div class="help-block">
                                <strong>{{ $errors->first('adresse') }}</strong>
                            </div>
                        @endif


                    </div>
                @endif

                @if(session()->has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">*
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        {{ session()->get('message') }}
                    </div>
                @endif
            </div>
            {{-- Information sur l'utilisateur --}}
            <div class="col-lg-12">
                <div class="card card-dark">
                    <div class="card-header">
                        <div class="row">
                            <div class="col">
                                <h3 class="card-title">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Info
                                            etablissement: {{$etablissement->name}}</font>
                                    </font>

                                </h3>


                            </div>

                            <div class="right">
                                <button type="submit" class="btn btn-warning " onclick="myFunction()">
                                    <font style="vertical-align: inherit; color:white; font-weigth:bold;">
                                        <font style="vertical-align: inherit;">Modifier</font>
                                    </font>
                                </button>

                            </div>
                        </div>

                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Nom</font>
                                    </font>
                                </label>
                                <input type="text" class="form-control" id="etablissementName" placeholder="Nom "
                                       value="{{$etablissement->name}} " disabled>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Adresse </font>
                                    </font>
                                </label>
                                <input type="email" class="form-control" id="etablissementAddresse" placeholder="Entrer une adresse"
                                       value="{{$etablissement->adresse}}" disabled>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">
                                <span style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">slug</font>
                                </span>
                                </label>
                                <input type="text" class="form-control" id="etablissementSlug" placeholder="Entrer le slug"value="{{$etablissement->slug}}" disabled>
                            </div>



                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Soumettre</font>
                                </font>
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        function myFunction() {
            document.getElementById("etablissementName").disabled = false;
            document.getElementById("etablissementAddresse").disabled = false;
            console.log('====================================');
            console.log('tester ');
            console.log('====================================');
        }

    </script>
@endsection
