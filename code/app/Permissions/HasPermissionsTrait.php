<?php


namespace App\Permissions;


use App\Models\Role;
use Illuminate\Support\Facades\DB;

trait HasPermissionsTrait {
        protected $role;
        protected $permissions;

    public function hasRole( ... $roles ) {
        foreach ($roles as $role) {
            if ($this->roles->where('slug', $role)) {
                return true;
            }
        }

        return false;
    }


//    public function permissions() {
//        return $this->belongsToMany(Permission::class,'users_permissions');
//    }

    public function hasRoleTo($user) {
       $role= DB::table('roles')->where('id', $user->role_id)->pluck('slug')->first();
       return $role;
    }

    public function giveRolesTo(... $roles) {
        // dd($roles);
        $rolesMod = Role::whereIn('slug', $roles)->get();

        if($rolesMod === null) {
            return $this;
        }

        $this->roles()->saveMany($rolesMod);

        return $this;
    }

   public function roles() {
        return $this->belongsTo(Role::class,'role_id');
    }

    public function deleteRoles( ... $roles ) {
        $roles = $this->getAllRoles($roles);
        $this->roles()->detach($roles);

        return $this;
    }

    public function givePermissionsTo(... $permissions) {
        $permissions = $this->getAllPermissions($permissions);
        dd($permissions);

        if($permissions === null) {
            return $this;
        }

        $this->permissions()->saveMany($permissions);

        return $this;
    }

    public function deletePermissions( ...$permissions ) {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);

        return $this;
    }

    protected function hasPermissionTo($permission) {
        // return $this->hasPermission($permission);
        return $this->hasPermissionThroughRole($permission) || $this->hasPermission($permission);
    }

    public function hasPermissionThroughRole($permission) {
        foreach ($permission->roles as $role){
            if($this->roles->contains($role)) {
                return true;
            }
        }

        return false;
    }

    protected function hasPermission($permission) {
        return (bool) $this->permissions->where('slug', $permission->slug)->count();
    }
}
