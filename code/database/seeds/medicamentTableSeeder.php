<?php

use App\Models\Medicaments;
use Illuminate\Database\Seeder;

class medicamentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Medicaments::create([
            'id'=>'123456987',
            'name'=>'Doliprane',
            'description'=>'anti douleur'

        ]);
    }
}
