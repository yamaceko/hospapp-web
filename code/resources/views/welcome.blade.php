<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- Required Meta Tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Page Title -->
    <title>Medino</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="medino/assets/images/logo/favicon.png" type="image/x-icon">

    <!-- CSS Files -->
    <link rel="stylesheet" href="medino/assets/css/animate-3.7.0.css">
    <link rel="stylesheet" href="medino/assets/css/font-awesome-4.7.0.min.css">
    <link rel="stylesheet" href="medino/assets/css/bootstrap-4.1.3.min.css">
    <link rel="stylesheet" href="medino/assets/css/owl-carousel.min.css">
    <link rel="stylesheet" href="medino/assets/css/jquery.datetimepicker.min.css">
    <link rel="stylesheet" href="medino/assets/css/linearicons.css">
    <link rel="stylesheet" href="medino/assets/css/style.css">
</head>

<body>
    <!-- Preloader Starts -->
    <div class="preloader">
        <div class="spinner"></div>
    </div>
    <!-- Preloader End -->

    <!-- Header Area Starts -->
    <header class="header-area">

        <div id="header" id="home">
            <div class="row align-items-center justify-content-between d-flex">
                <div id="logo" style="margin-left: 80px;">
                    <a href="index.html"><img src="medino/assets/images/logo/logo.png" alt="" title="" /></a>
                </div>
                <div class="container">
                    <div class="row align-items-center justify-content-between d-flex">
                        <div id="logo">
                            <a href="index.html"><img src="assets/images/logo/logo.png" alt="" title="" /></a>
                        </div>
                        <nav id="nav-menu-container">
                            <ul class="nav-menu">

                                @auth
                                <li><a href="departments.html">Application</a></li>
                                <li>
                                    <a href="{{ route('logout') }}" class="dropdown-item dropdown-footer"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        déconexion</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                                @endauth
                            </ul>
                        </nav><!-- #nav-menu-container -->
                    </div>
                </div>
            </div>
    </header>
    <!-- Header Area End -->

    <!-- Banner Area Starts -->
    <section class="banner-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <h4>Hospi App</h4>
                    <h1>Application de gestion médical</h1>
                    <p>Nous vous aidons à mieux gerer votre stock de médicaments</p>
                    @guest
                    <a href="{{ route('login') }}" class="template-btn mt-3">Se connecter</a>
                    @endguest


                </div>
            </div>
        </div>
    </section>
    <!-- Banner Area End -->

    <!-- Footer Area Starts -->
    <footer class="footer-area section-padding">

        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-6">
                        <span>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                                document.write(new Date().getFullYear());

                            </script> All rights reserved | developper avec <i class="fa fa-heart-o"
                                aria-hidden="true"></i> par Moi
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </span>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="social-icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Area End -->


    <!-- Javascript -->
    <script src="medino/assets/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="medino/assets/js/vendor/bootstrap-4.1.3.min.js"></script>
    <script src="medino/assets/js/vendor/wow.min.js"></script>
    <script src="medino/assets/js/vendor/owl-carousel.min.js"></script>
    <script src="medino/assets/js/vendor/jquery.datetimepicker.full.min.js"></script>
    <script src="medino/assets/js/vendor/jquery.nice-select.min.js"></script>
    <script src="medino/assets/js/vendor/superfish.min.js"></script>
    <script src="medino/assets/js/main.js"></script>

</body>

</html>
