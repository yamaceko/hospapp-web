@extends('apps.layout')

@section('title')
    Developer | Dashboard
@endsection
@section('breadcrumb-title')
    Dashboard Developer
@endsection

@section('aside-menu')
    @include('developer/_menu')
@endsection
