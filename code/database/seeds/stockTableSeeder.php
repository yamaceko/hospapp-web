<?php

use App\Models\Stock;
use Illuminate\Database\Seeder;

class stockTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Stock::create([
            'slug'=>'stock-doliprane',
            'medicament_id'=>'123456987'
        ]);
        //
    }
}
