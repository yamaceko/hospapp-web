<?php

use App\Models\Etablissement;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $etablissement_cocody=Etablissement::where('slug','chu-cocody')->first();
        $dev_role = Role::where('slug','developer')->first();



        User::create([
            'name'=>'marc0',
            'number_phone'=>'89383346',
            'email'=>'yamaceko2891@gmail.com',
            'password'=>bcrypt('Test01')
        ])->etablissements()->attach($etablissement_cocody);
        User::create([
            'name'=>'radicalc3d',
            'number_phone'=>'49987652',
            'email'=>'radicalc3d@gmail.com',
            'password'=>bcrypt('Test01')
        ])->etablissements()->attach($etablissement_cocody);
    }
}
