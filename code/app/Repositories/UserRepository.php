<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class UserRepository extends BaseRepository
{
    protected  $user;
    public function  __construct(User $user)
    {
        $this->model=$user;

    }
    /**
     * Paginate and rate.
     *
     * @param  Builder  $query
     * @return LengthAwarePaginator
     */


    public function queryUserAndEtablissement (User $user)
    {
        return $user->with('user','etablissements');

    }
    public function getPaginate($n)
    {
        return $this->user->paginate($n);
    }
    public function getAllUser()
    {
        return $this->user->pagniate(15);
    }

    public function update($id, Array $inputs)
    {
        $this->save($this->getById($id), $inputs);
    }

    public function getById($id)
    {
        return $this->user->findOrFail($id);
    }

    public function destroy($id)
    {
        $this->getById($id)->delete();
    }


}
