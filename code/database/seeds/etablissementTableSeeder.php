<?php

use App\Models\Etablissement;
use App\Models\Role;
use Illuminate\Database\Seeder;

class etablissementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Etablissement::create([
            'name'=>'CHU Cocody',
            'adresse'=>'Cocody',
            'slug'=>'chu-cocody'
        ]);
        Etablissement::create([
            'name'=>'CHU Yopougon',
            'adresse'=>'Yopougon',
            'slug'=>'chu-youpougon'
        ]);
        Etablissement::create([
            'name'=>'CHU Trechville',
            'adresse'=>'Trechville',
            'slug'=>'chu-trechville'
        ]);
        Etablissement::create([
            'name'=>'CHU Angre',
            'adresse'=>'Angre',
            'slug'=>'chu-angre'
        ]);

    }
}
