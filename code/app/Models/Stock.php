<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Stock extends Model
{
    use Notifiable;
    use Uuids;
    public $incrementing = false;

    protected $fillable = [
        'slug','total_quantity','medicament_id'
    ];
    public  function medicaments(){
        return $this->belongsTo(Medicaments::class,'medicament_id');
    }
    public function operations(){
        return $this->hasMany(Operation::class,'stock_id');
    }
    public function getMed($id){
        return $medicaments=Medicaments::where('id',$id)->first();
    }

}
