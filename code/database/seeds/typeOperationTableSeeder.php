<?php

use App\Models\TypeOperation;
use Illuminate\Database\Seeder;

class typeOperationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeOperation::create([
            'slug'=>'operation-in',
            'name'=>'incoming'
        ]);
        TypeOperation::create([
            'slug'=>'operation-out',
            'name'=>'out'
        ]);
        //
    }
}
