<?php

use App\Models\Role;
use Illuminate\Database\Seeder;
use App\Models\Permission;

class permissionTableSeeder extends Seeder
{
    /**
     * fr: j'utilise cette classe pour mettre des infos par defaut dans la base de donnée
     * ici je definis ce que chaque type d'utlisateur peux faire
     */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dev_role = Role::where('slug','developer')->first();
        $admin_role = Role::where('slug','admin')->first();
        $user_role = Role::where('slug','user')->first();

        //administrateur permission
        Permission::create([
            'name'=>'create user',
            'slug'=>'create-user'
        ])->roles()->attach($admin_role);

        Permission::create([
            'name'=>'edit user',
            'slug'=>'edit-user'
        ])->roles()->attach($admin_role);

        Permission::create([
            'name'=>'delete user',
            'slug'=>'delete-user'
        ])->roles()->attach($admin_role);
        //<-- manage etablissement-->
        Permission::create([
            'name'=>'create etablissement',
            'slug'=>'create-etablissement'
        ])->roles()->attach($admin_role);

        Permission::create([
            'name'=>'edit etablissement',
            'slug'=>'edit-etablissement'
        ])->roles()->attach($admin_role);

        Permission::create([
            'name'=>'delete etablissement',
            'slug'=>'delete-etablissement'
        ])->roles()->attach($admin_role);
        //<-- end manage etablissement-->
        //<-- manage stock-->
        Permission::create([
            'name'=>'create stock',
            'slug'=>'create-stock'
        ])->roles()->attach($admin_role);

        Permission::create([
            'name'=>'edit stock',
            'slug'=>'edit-stock'
        ])->roles()->attach($admin_role);

        Permission::create([
            'name'=>'delete stock',
            'slug'=>'delete-stock'
        ])->roles()->attach($admin_role);
        //<-- end manage stock-->
        //<-- manage medicament-->
        Permission::create([
            'name'=>'create medicament',
            'slug'=>'create-medicament'
        ])->roles()->attach($admin_role);

        Permission::create([
            'name'=>'edit medicament',
            'slug'=>'edit-medicament'
        ])->roles()->attach($admin_role);

        Permission::create([
            'name'=>'delete medicament',
            'slug'=>'delete-medicament'
        ])->roles()->attach($admin_role);
        //<-- end manage medicament-->
        //<-- manage operation-->
        Permission::create([
            'name'=>'create operation',
            'slug'=>'create-operation'
        ])->roles()->attach($admin_role);

        Permission::create([
            'name'=>'edit operation',
            'slug'=>'edit-operation'
        ])->roles()->attach($admin_role);

        Permission::create([
            'name'=>'delete operation',
            'slug'=>'delete-operation'
        ])->roles()->attach($admin_role);
        //<-- end manage operation-->



        //developer role
        //<-- manage user-->
        Permission::create([
            'name'=>'create user',
            'slug'=>'create-user'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'edit user',
            'slug'=>'edit-user'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'delete user',
            'slug'=>'delete-user'
        ])->roles()->attach($dev_role);
        //<-- end manage user-->
        //<-- manage admin-->
        Permission::create([
            'name'=>'create admin',
            'slug'=>'create-admin'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'edit admin',
            'slug'=>'edit-admin'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'delete admin',
            'slug'=>'delete-admin'
        ])->roles()->attach($dev_role);
        //<-- end manage admin-->
        //<-- manage role-->
        Permission::create([
            'name'=>'create role',
            'slug'=>'create-role'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'edit role',
            'slug'=>'edit-role'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'delete role',
            'slug'=>'delete-role'
        ])->roles()->attach($dev_role);
        //<-- end manage admin-->
        //<-- manage permission-->
        Permission::create([
            'name'=>'create permission',
            'slug'=>'create-permission'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'edit permission',
            'slug'=>'edit-permission'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'delete permission',
            'slug'=>'delete-permission'
        ])->roles()->attach($dev_role);
        //<-- end manage admin-->
        //<-- manage etablissement-->
        Permission::create([
            'name'=>'create etablissement',
            'slug'=>'create-etablissement'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'edit etablissement',
            'slug'=>'edit-etablissement'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'delete etablissement',
            'slug'=>'delete-etablissement'
        ])->roles()->attach($dev_role);
        //<-- end manage etablissement-->
        //<-- manage stock-->
        Permission::create([
            'name'=>'create stock',
            'slug'=>'create-stock'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'edit stock',
            'slug'=>'edit-stock'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'delete stock',
            'slug'=>'delete-stock'
        ])->roles()->attach($dev_role);
        //<-- end manage stock-->
        //<-- manage medicament-->
        Permission::create([
            'name'=>'create medicament',
            'slug'=>'create-medicament'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'edit medicament',
            'slug'=>'edit-medicament'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'delete medicament',
            'slug'=>'delete-medicament'
        ])->roles()->attach($dev_role);
        //<-- end manage medicament-->
        //<-- manage operation-->
        Permission::create([
            'name'=>'create operation',
            'slug'=>'create-operation'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'edit operation',
            'slug'=>'edit-operation'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'delete operation',
            'slug'=>'delete-operation'
        ])->roles()->attach($dev_role);
        //<-- end manage operation-->
        //<-- manage operation-->
        Permission::create([
            'name'=>'create type operation',
            'slug'=>'create-type-operation'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'edit type operation',
            'slug'=>'edit-type-operation'
        ])->roles()->attach($dev_role);

        Permission::create([
            'name'=>'delete type operation',
            'slug'=>'delete-type-operation'
        ])->roles()->attach($dev_role);
        //<-- end manage type operation-->

        //user role


        //<-- manage operation-->
        Permission::create([
            'name'=>'create operation',
            'slug'=>'create-operation'
        ])->roles()->attach($user_role);

    }
}
