<?php

namespace App\Http\Controllers;

use App\Models\Etablissement;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use mysql_xdevapi\Exception;

class DeveloperController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $currentUser = auth()->user();
        $role=$currentUser->hasRoleTo($currentUser);

      
            $u=User::class;
            $limit = 20;
            $userQuery = User::orderBy('created_at', 'desc');
            $users = null;
            $roles=Role::all();
            $etablissements=Etablissement::all();
            $u=null;
            $users = $userQuery->paginate($limit);// recupere la liste des roles  et injections dans la base de donnée
            //$users=User::All()->paginate(10);
            \Debugbar::warning(['users' => $users]);
            \Debugbar::warning(['etablissements' => $etablissements]);
            return view('developer/index',['users' => $users,'roles'=>$roles,'etablissements'=>$etablissements]); //passe le tableaux d'utilisateur a la vue


        

        

       
        //
    }
    // Fonction de validation des informations utilisateur
    protected function addUservalidator(array $data)
    {
        return Validator::make($data, [

            'name' => 'required|string|max:255',
            'role'=>'required|integer',
            // 'phone_number' => 'required|numeric|digits_between:8,16|unique:users',
            'number_phone' => 'required|string|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',

        ]);
    }




    public function create(Request $request)
    {
        $this->addUservalidator($request->all())->validate();

        try{
            \DB::beginTransaction();

            // Création en base de donnée du nouvel utilisateur
          $user = User::create([
                'name' => $request->input('name'),
                'number_phone' => $request->input('number_phone'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'role_id'=>$request->input('role'),

          ]);
        
        //   echo $user;
          $user->save();
           \DB::commit();
            \Debugbar::warning(['users' => $request]);
             return redirect()
                 ->back()
                 ->with('message', "Utilisateur ajouté avec succès");
                 \Debugbar::warning(['users' => $request]);
        }catch (\Exception $exception){
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(array('message' => $exception->getMessage()));

        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function showUser(Request $request, $id){

        $limit = 10;

        // Récuperation de l'utilisateur
        $user = User::whereId($id)->first();
        $roles=Role::all();
        $etablissements=Etablissement::all();

        if(is_null($user)) {
            return "Utilisateur invalide";
        }

        return view('developer/show', [
            'user' => $user,
            'roles' => $roles,
            'etablissements' => $etablissements,
            'limit' => $limit,
        ]);


    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    // Suppression d'un utilisateur
	public function removeUser(Request $request, $id)
	{
		// Suppression
		try {
			$user = User::where('id', $id)->delete();

			return redirect()
				->back()
				->with('message', "Utilisateur supprimer avec succès");

		} catch (\Exception $e) {
			return redirect()
				// ->route('users.index')
				->back()
				->withInput()
				->withErrors(array('message' => "Une erreur c'est produite lors de la suppression de l'utilisateur. ".$e->getMessage()));
		}
	}
}
