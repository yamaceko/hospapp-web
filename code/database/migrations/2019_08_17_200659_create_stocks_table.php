<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->uuid('id')->unique()->primary();
            $table->string('slug');
            $table->double('total_quantity')->default(0);
            $table->string('medicament_id')->unique();
            $table->timestamps();

            //foreign key

           $table->foreign('medicament_id')->references('id')->on('medicaments');
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
