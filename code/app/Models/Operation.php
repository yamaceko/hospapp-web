<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    protected $fillable = [
        'quantity','description'
    ];

    public function stock(){
        return $this->belongsTo(Stock::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function typeOperation(){
        return $this->belongsTo(TypeOperation::class);
    }
}
