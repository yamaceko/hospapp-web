@extends('apps.layout')

@section('title')
    Developer | Dashboard
@endsection
@section('breadcrumb-title')
    Dashboard Developper
@endsection

@section('aside-menu')
    @include('developer/_menu')
@endsection


@section('content')

    <section class="content">
        @if(Auth::user()->hasRoleTo(Auth::user())=='developer')
            <div class="row">
                <div class="col-lg-12">
                    @if (isset($errors) && count($errors) > 0)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            @if ($errors->has('message'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </div>
                            @endif

                            @if ($errors->has('name'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                            @endif

                            @if ($errors->has('adresse'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('adresse') }}</strong>
                                </div>
                            @endif
                            @if ($errors->has('slug'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('slug') }}</strong>
                                </div>
                            @endif


                        </div>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">*
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            {{ session()->get('message') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Liste des etablissements</h3>

                    <div class="card-tools">
                        <a href="#" class="btn btn-primary btn-sm" title="Ajouter une permission" data-toggle="modal"
                           data-target="#modalAddEtablissementForm">
                            <i class="fas fa-plus"></i> Ajouter d'un etablissement
                        </a>
                    </div>
                </div>


                <div class="card-body table-responsive p-0">
                    @if (count($etablissements) > 0)
                        <style>
                            .active-cyan-2 input[type=text]:focus:not([readonly]) {
                                border-bottom: 1px solid #4dd0e1;
                                box-shadow: 0 1px 0 0 #4dd0e1;
                            }

                            .active-cyan input[type=text] {
                                border-bottom: 1px solid #4dd0e1;
                                box-shadow: 0 1px 0 0 #4dd0e1;
                            }

                            .active-cyan .fa,
                            .active-cyan-2 .fa {
                                color: #4dd0e1;
                            }

                        </style>

                        <!-- Search form -->
                        <div class="container">
                            <div class="row p-4">
                                <div class="col-sm-12">
                                    <form class="form-inline active-cyan-4 justify-content-center">
                                        <input class="form-control form-control-lg mr-3 w-75" type="text" placeholder="Recherche..."
                                               aria-label="Search">
                                        <i class="fas fa-search" aria-hidden="true"></i>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <table id="example1" class="table table-striped table-valign-middle">
                            <thead>
                            <tr>


                                <th>Nom </th>
                                <th>Adresse</th>
                                <th>Slug</th>
                                <th>Créé le</th>
                                <th>modifié le</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($etablissements as $etablissement)
                                <tr>
                                    <td>{{ $etablissement->name }}</td>
                                    <td>{{ $etablissement->addresse }}</td>
                                    <td>{{ $etablissement->slug }}</td>


                                    <td>{{ $etablissement->created_at }}</td>



                                        <td>{{ $etablissement->updated_at }}</td>



                                    <td class="text-right">

                                        <a href="#" title="Afficher fiche utilisateur"
                                           class="btn btn-outline-primary btn-sm"><i class="fas fa-folder"></i></a>

                                        <a href="#" title="Supprimer" class="btn btn-outline-danger btn-sm ml-3" data-toggle="modal" data-target="#modalRemoveID{{$etablissement->id}}"><i class="fas fa-trash"></i></a>


                                        {{-- Delete modale --}}
                                        <div class="modal fade" id="modalRemoveID{{$etablissement->id}}" tabindex="-1" role="dialog" aria-labelledby="modalRemoveID{{$etablissement->id}}" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <form class="modal-content form-horizontal" method="POST" action="#">
                                                    {{ csrf_field() }}

                                                    <div class="modal-header text-center">
                                                        <h4 class="modal-title w-100 font-weight-bold">Suppression de l'etablissement {{ $etablissement->id }}</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            Voulez vous vraiment supprimer cet etablissement ? <br />
                                                            En appuyant sur "<strong>Supprimer maintenant</strong>" l'utilisateur et toutes les informations en relation seront definitivement supprimer.
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-danger">Supprimer maintenant</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </td>


                                </tr>
                            @endforeach
                            </tbody>

                            <tfoot>
                            <tr>
                                <th>{{ $etablissements->links() }}</th>
                            </tr>
                            </tfoot>
                        </table>
                    @else

                    @endif
                </div>
            </div>
            {{-- Add etablissement --}}
            <div class="modal fade" id="modalAddEtablissementForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <form class="modal-content form-horizontal" method="POST" action="#">
                        {{ csrf_field() }}

                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold">Création d'un utilisateur</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <!-- Nom -->
                            <div class="form-group{{ $errors->has('names') ? ' has-error' : '' }}">
                                <label for="last_name" class="col-md-4 control-label">Nom </label>

                                <div class="col-lg-12">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Nom" name="name"
                                               value="{{ old('name') }}" required autofocus>

                                        <div class="input-group-append input-group-text">
                                            <span class="fas fa-user"></span>
                                        </div>
                                    </div>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>
                            <!-- Email -->

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Email</label>

                                <div class="col-lg-12">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Email" name="email"
                                               value="{{ old('email') }}" required>

                                        <div class="input-group-append input-group-text">
                                            <span class="fas fa-at"></span>
                                        </div>
                                    </div>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <!-- Phone number -->
                            <div class="form-group{{ $errors->has('number_phone') ? ' has-error' : '' }}">
                                <label for="number_phone" class="col-md-4 control-label">Numéro de téléphone</label>

                                <div class="col-lg-12">
                                    <div class="input-group mb-3">
                                        <input type="number" class="form-control" placeholder="Numéro de téléphone"
                                               name="number_phone" value="{{ old('number_phone') }}" required>

                                        <div class="input-group-append input-group-text">
                                            <span class="fas fa-phone"></span>
                                        </div>
                                    </div>

                                    @if ($errors->has('number_phone'))
                                        <span class="help-block">
                                <strong>{{ $errors->first('number_phone') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>



                        </div>


                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default">Ajouter un utilisateur</button>
                        </div>
                    </form>
                </div>
            </div>
        @else
            <p>vous n'estes pas un developer </p>

        @endif

    </section>

@endsection
