<?php

namespace App\Http\Controllers;

use App\Models\Etablissement;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EtablissementController extends Controller
{

    // Fonction de validation des informations utilisateur
    protected function EtablissementUservalidator(array $data)
    {
        return Validator::make($data, [

            'name' => 'required|string|max:255',
            'adresse'=>'required|string|max:255'

        ]);
    }

    public function index(Request $request){
        $limit = 20;
        $currentUser = auth()->user();
        //return view('developer/etablissement');
        $etablissementQuery = Etablissement::orderBy('created_at', 'desc');
        $etablissements=$etablissementQuery->paginate($limit);

        \Debugbar::warning(['etablissements' => $etablissements]);
        return view('etablissement/index',['etablissements'=>$etablissements]); //passe le tableaux d'utilisateur a la vue


    }
    public function create(Request $request)
    {
        $this->EtablissementUservalidator($request->all())->validate();

        try{
            \DB::beginTransaction();

            // Création en base de donnée du nouvel utilisateur
            $etablissement = Etablissement::create([
                'name' => $request->input('name'),
                'adresse' => $request->input('adresse'),
                'slug' =>  Str::slug($request->input('name')),


            ]);

            //   echo $user;
            $etablissement->save();
            \DB::commit();
            \Debugbar::warning(['users' => $request]);
            return redirect()
                ->back()
                ->with('message', "l'etablissement ajouté avec succès");
            \Debugbar::warning(['users' => $request]);
        }catch (\Exception $exception){
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(array('message' => $exception->getMessage()));

        }
    }
    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function showEtablissement(Request $request, $id){

        $limit = 10;

        // Récuperation de l'etablissemnt
        $etablissement = Etablissement::whereId($id)->first();


        if(is_null($etablissement)) {
            return "etablissement invalide";
        }

        return view('etablissement/show', [

            'etablissement' => $etablissement,
            'limit' => $limit,
        ]);


    }


    // Suppression d'un utilisateur

    /**
     * use this function to remove
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeEtablissement(Request $request, $id)
    {
        // Suppression
        try {
            //remove etablssement to database
            Etablissement::where('id', $id)->delete();

            return redirect()
                ->back()
                ->with('message', "Etablissement supprimer avec succès");

        } catch (\Exception $e) {
            return redirect()
                // ->route('users.index')
                ->back()
                ->withInput()
                ->withErrors(array('message' => "Une erreur c'est produite lors de la suppression de l'etablissement. ".$e->getMessage()));
        }
    }

}
