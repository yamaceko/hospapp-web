<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeOperation extends Model
{
    protected $fillable = [
        'name', 'slug',
    ];
    public function operations(){
        return $this->hasMany(Operation::class,'type_operation_id');
    }
}
