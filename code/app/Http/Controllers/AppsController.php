<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AppsController extends Controller
{



    protected $userRepo;

    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $this->middleware('auth');
    }
    // Page d'accueil de l'application

    public function index(Request $request)
    {
        $currentUser = auth()->user();
        $role=$currentUser->hasRoleTo($currentUser);
        if($role=='admin')
        {   \Debugbar::warning($role);
            log ($currentUser->role);
            return view('apps/index');
        }elseif ($role=='developer')
        {   return redirect()
                ->route('developer');
            \Debugbar::info($role);
        }
        
        \Debugbar::warning($role);
          return view('welcome');
    }

}
