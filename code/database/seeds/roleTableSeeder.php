<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class roleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Administrteur
        Role::create([
            'name'=>'admin',
            'slug'=>'admin'
        ]);

        //developer
        Role::create([
            'name'=>'developer',
            'slug'=>'developer'
        ]);

        //user
        Role::create([
            'name'=>'user',
            'slug'=>'user'
        ]);

    }
}
