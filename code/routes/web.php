<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Page d'accueil de l'application
Route::get('/apps', 'AppsController@index')->name('apps.index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//gestions des utilisateurs
Route::resource('user', 'UserController');

//------------------------------route pour les  developpers-----------------------------------
Route::get('/developer', 'DeveloperController@index')
    ->name('developer')
    ->middleware('auth');
Route::post('/developer','DeveloperController@create')
    ->name('users.add')
    ->middleware('auth');
// User Show information
Route::get('/developer/{id}/show', 'developerController@showUser')
    ->name('users.show')
    ->middleware('auth')
;
// Users suppression d'un utilisateur
Route::post('/developer/{id}/delete', 'developerController@removeUser')
	->name('users.delete')
	->middleware('auth')
;
//gestions des etablissements

Route::get('/etablissement', 'EtablissementController@index')
    ->name('etablissement')
    ->middleware('auth');
Route::get('/etablissement/{id}/show', 'EtablissementController@showEtablissement')
    ->name('etablissements.show')
    ->middleware('auth')
;
Route::post('/etablissement','EtablissementController@create')
    ->name('etablissements.add')
    ->middleware('auth');
// Users suppression d'un utilisateur
Route::post('/etablissement/{id}/delete', 'EtablissementController@removeEtablissement')
    ->name('etablissements.delete')
    ->middleware('auth')
;

//gestions des médicaments

Route::get('/medicament', 'MedicamentController@index')
    ->name('medicament')
    ->middleware('auth');
Route::get('/medicament/{id}/show', 'MedicamentController@show')
    ->name('medicaments.show')
    ->middleware('auth')
;
Route::post('/medicament','MedicamentController@store')
    ->name('medicaments.add')
    ->middleware('auth');
// Users suppression d'un utilisateur
Route::post('/medicament/{id}/delete', 'MedicamentController@destroy')
    ->name('medicaments.delete')
    ->middleware('auth')
;
//gestions des Stock

Route::get('/stock', 'stockController@index')
    ->name('stock')
    ->middleware('auth');
Route::get('/stock/{id}/show', 'stockController@show')
    ->name('stocks.show')
    ->middleware('auth')
;
Route::post('/stock','stockController@store')
    ->name('stocks.add')
    ->middleware('auth');
// Users suppression d'un utilisateur
Route::post('/stock/{id}/delete', 'stockController@destroy')
    ->name('stocks.delete')
    ->middleware('auth')
;

//gestions des operations
Route::get('/operation', 'operationController@index')
    ->name('operations')
    ->middleware('auth');
Route::get('/operation/{id}/show', 'operationController@show')
    ->name('operations.show')
    ->middleware('auth')
;
Route::post('/operation','operationController@store')
    ->name('operations.add')
    ->middleware('auth');
// Users suppression d'une operation
Route::post('/operation/{id}/delete', 'operationController@destroy')
    ->name('operations.delete')
    ->middleware('auth')
;
