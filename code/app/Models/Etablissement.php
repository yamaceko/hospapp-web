<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etablissement extends Model
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    use Uuids;
    protected $fillable = [
        'name', 'adresse','slug',
    ];
    public function users(){
        return $this->belongsToMany(UserEtablissement::class,'user');
    }
}
