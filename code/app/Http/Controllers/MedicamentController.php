<?php

namespace App\Http\Controllers;

use App\Models\Etablissement;
use App\Models\Medicaments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MedicamentController extends Controller
{


    // Fonction de validation des informations utilisateur
    protected function Medicamentvalidator(array $data)
    {
        return Validator::make($data, [
            'code_bar' => 'required|string|max:25|unique:medicaments',
            'name' => 'required|string|max:255',
            'description'=>'required|string|max:255'

        ]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = 20;
        $currentUser = auth()->user();
        //return view('developer/etablissement');
        $medicamentQuery = Medicaments::orderBy('created_at', 'desc');
        $medicaments=$medicamentQuery->paginate($limit);

        \Debugbar::warning(['medicaments' => $medicaments]);
        return view('medicaments/index',['medicaments'=>$medicaments]); //passe le tableaux d'utilisateur a la vue


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->Medicamentvalidator($request->all())->validate();

        try{
            \DB::beginTransaction();

            // Création en base de donnée du nouvel utilisateur
            $medicament = Medicaments::create([
                'code_bar' => $request->input('code_bar'),
                'name' => $request->input('name'),
                'description' =>  Str::slug($request->input('description')),


            ]);

            //   echo $user;
            $medicament->save();
            \DB::commit();
            \Debugbar::warning(['medicament' => $request]);
            return redirect()
                ->back()
                ->with('message', "médicament ajouté avec succès");
            \Debugbar::warning(['medicament' => $request]);
        }catch (\Exception $exception){
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(array('message' => $exception->getMessage()));

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $limit = 10;

        // Récuperation du médicament
        $medicament = Medicaments::whereId($id)->first();


        if(is_null($medicament)) {
            return "medicament invalide";
        }

        return view('medicaments/show', [

            'medicament' => $medicament,
            'limit' => $limit,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) //---ici l'id est le code bar
    {
        // Suppression
        try {
            //remove medicament to database
            Medicaments::where('code_bar', $id)->delete();

            return redirect()
                ->back()
                ->with('message', "Medicament supprimer avec succès");

        } catch (\Exception $e) {
            return redirect()
                // ->route('users.index')
                ->back()
                ->withInput()
                ->withErrors(array('message' => "Une erreur c'est produite lors de la suppression de l'etablissement. ".$e->getMessage()));
        }
    }
}
