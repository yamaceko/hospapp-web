<?php

namespace App\Http\Controllers;

use App\Models\Etablissement;
use App\Models\Medicaments;
use App\Models\Role;
use App\Models\Stock;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class StockController extends Controller
{

    // Fonction de validation des informations stock
    protected function addStockvalidator(array $data)
    {
        return Validator::make($data, [

            'slug' => 'required|string|max:255',
            'medicament_id' => 'required|string|unique:stocks',


        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = auth()->user();




        $limit = 20;
        $stockQuery = Stock::orderBy('created_at', 'desc');
        $stocks = null;
        $medicaments=Medicaments::all();
        $stocks = $stockQuery->paginate($limit);// recupere la liste des roles  et injections dans la base de donnée
        //$users=User::All()->paginate(10);
        \Debugbar::warning(['stocks' => $stocks]);
        return view('stocks/index',['stocks' => $stocks,'medicaments'=>$medicaments]); //passe le tableaux d'utilisateur a la vue







        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->addStockvalidator($request->all())->validate();
        try{
            \DB::beginTransaction();

            // Création en base de donnée du nouvel utilisateur
            $stock = Stock::create([
                'slug' => Str::slug($request->input('slug')),
                'medicament_id'=>$request->input('medicament_id'),

            ]);
            echo $stock;

            //   echo $user;
            $stock->save();
            \DB::commit();
            \Debugbar::warning(['stock' => $request]);
            return redirect()
                ->back()
                ->with('message', "stock ajouté avec succès");
        }catch (\Exception $exception){
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(array('message' => $exception->getMessage()));

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
