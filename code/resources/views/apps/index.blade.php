@extends('apps.layout')

@section('title')
    Utilisateur | Dashboard
@endsection

@section('breadcrumb-title')
    Tableau de bord
@endsection

@section('aside-menu')
    @include('apps/_menu')
@endsection

@section('content')

    <p> {{Auth::user() }} </p>

@endsection
