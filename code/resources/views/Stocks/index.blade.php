@extends('apps.layout')

@section('title')
    Developer | Dashboard
@endsection
@section('breadcrumb-title')
    Dashboard Developer
@endsection

@section('aside-menu')
    @include('developer/_menu')
@endsection


@section('content')

    <section class="content">
        @if(Auth::user()->hasRoleTo(Auth::user())=='developer')
            <div class="row">
                <div class="col-lg-12">
                    @if (isset($errors) && count($errors) > 0)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            @if ($errors->has('message'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </div>
                            @endif

                            @if ($errors->has('slug'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('slug') }}</strong>
                                </div>
                            @endif

                            @if ($errors->has('medicament'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('medicament') }}</strong>
                                </div>
                            @endif


                        </div>
                    @endif

                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">*
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            {{ session()->get('message') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Liste des stocks</h3>

                    <div class="card-tools">
                        <a href="#" class="btn btn-primary btn-sm" title="Ajouter un stock" data-toggle="modal"
                           data-target="#modalAddStockForm">
                            <i class="fas fa-plus"></i> Ajouter un stock
                        </a>
                    </div>
                </div>


                <div class="card-body table-responsive p-0">
                    @if (count($stocks) > 0)
                        <style>
                            .active-cyan-2 input[type=text]:focus:not([readonly]) {
                                border-bottom: 1px solid #4dd0e1;
                                box-shadow: 0 1px 0 0 #4dd0e1;
                            }

                            .active-cyan input[type=text] {
                                border-bottom: 1px solid #4dd0e1;
                                box-shadow: 0 1px 0 0 #4dd0e1;
                            }

                            .active-cyan .fa,
                            .active-cyan-2 .fa {
                                color: #4dd0e1;
                            }

                        </style>

                        <!-- Search form -->
                        <div class="container">
                            <div class="row p-4">
                                <div class="col-sm-12">
                                    <form class="form-inline active-cyan-4 justify-content-center">
                                        <input class="form-control form-control-lg mr-3 w-75" type="text" placeholder="Recherche..."
                                               aria-label="Search">
                                        <i class="fas fa-search" aria-hidden="true"></i>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <table id="example1" class="table table-striped table-valign-middle">
                            <thead>
                            <tr>


                                <th>Numero Stock</th>
                                <th>Nom</th>
                                <th>Quantité</th>
                                <th>Medicament</th>
                                <th>Créé le</th>
                                <th>modifié le</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($stocks as $stock)
                                <tr>
                                    <td>{{ $stock->id }}</td>
                                    <td>{{ $stock->slug }}</td>
                                    <td>{{ $stock->total_quantity }}</td>
                                    <td>
                                        {{-- affiche les roles si ceci son disponible --}}
                                        @if( !is_null($stock->getMed($stock->medicament_id )))
                                            {{ $stock->getMed($stock->medicament_id)->name }}

                                        @endif
                                    </td>


                                    <td>{{ $stock->created_at }}</td>


                                    @if( !is_null($stock->updated_at))
                                        <td>{{ $stock->updated_at }}</td>
                                    @endif


                                    <td class="text-right">

                                        <a href="{{ route('stocks.show', $stock->id) }}" title="Afficher fiche stock"
                                           class="btn btn-outline-primary btn-sm"><i class="fas fa-folder"></i></a>

                                        <a href="#" title="Supprimer" class="btn btn-outline-danger btn-sm ml-3" data-toggle="modal" data-target="#modalRemoveID{{$stock->id}}"><i class="fas fa-trash"></i></a>


                                        {{-- Delete modale --}}
                                        <div class="modal fade" id="modalRemoveID{{$stock->id}}" tabindex="-1" role="dialog" aria-labelledby="modalRemoveID{{$stock->id}}" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <form class="modal-content form-horizontal" method="POST" action="{{ route('stocks.delete', ['id' => $stock->id]) }}">
                                                    {{ csrf_field() }}

                                                    <div class="modal-header text-center">
                                                        <h4 class="modal-title w-100 font-weight-bold">Suppression de l'utilisateur {{ $stock->slug }}</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="text-center">
                                                            Voulez vous vraiment supprimer cet stock ? <br />
                                                            En appuyant sur "<strong>Supprimer maintenant</strong>" l'utilisateur et toutes les informations en relation seront definitivement supprimer.
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-danger">Supprimer maintenant</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </td>


                                </tr>
                            @endforeach
                            </tbody>

                            <tfoot>
                            <tr>
                                <th>{{ $stocks->links() }}</th>
                            </tr>
                            </tfoot>
                        </table>
                    @else

                    @endif
                </div>
            </div>
            {{-- Add stock --}}
            <div class="modal fade" id="modalAddStockForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <form class="modal-content form-horizontal" method="POST" action="{{ route('stocks.add') }}">
                        {{ csrf_field() }}

                        <div class="modal-header text-center">
                            <h4 class="modal-title w-100 font-weight-bold">Création d'un Stock</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <!-- Nom -->
                            <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                <label for="slug" class="col-md-4 control-label">Nom Stock</label>

                                <div class="col-lg-12">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Nom stock" name="slug"
                                               value="{{ old('slug') }}" required autofocus>

                                        <div class="input-group-append input-group-text">
                                            <span class="fas fa-user"></span>
                                        </div>
                                    </div>

                                    @if ($errors->has('slug'))
                                        <span class="help-block">
                                <strong>{{ $errors->first('slug') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('medicament') ? ' has-error' : '' }}">
                                <label for="medicament" class="col-md-4 control-label">Medicament</label>
                                <div class="col-lg-12">
                                    <select id="medicament" name="medicament_id" class="form-control">
                                        @foreach($medicaments as $medicament)
                                            <option value="{{ $medicament->id }}">{{ $medicament->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <!-- Quantité total -->
                            <div class="form-group{{ $errors->has('total_quantity') ? ' has-error' : '' }}">
                                <label for="number_phone" class="col-md-4 control-label">Quantité en stock</label>

                                <div class="col-lg-12">
                                    <div class="input-group mb-3">
                                        <input type="number" class="form-control" placeholder=" 0 "
                                               name="total_quantity" value="{{ old('total_quantity') }}" disabled>

                                        <div class="input-group-append input-group-text">
                                            <span class="fas fa-sort-numeric-up-alt"></span>
                                        </div>
                                    </div>

                                    @if ($errors->has('total_quantity'))
                                        <span class="help-block">
                                <strong>{{ $errors->first('total_quantity') }}</strong>
                            </span>
                                    @endif
                                </div>
                            </div>




                        </div>


                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default">Ajouter un stock</button>
                        </div>
                    </form>
                </div>
            </div>
        @else
            <p>vous n'estes pas un developer </p>

        @endif

    </section>

@endsection
