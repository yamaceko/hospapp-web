<li class="nav-item">
    <a href="{{ route('apps.index') }}" class="nav-link @if(Route::current()->getName() == 'apps.index') active @endif">
        <i class="nav-icon fas fa-th"></i>

        <p>
            ACCUEIL
            {{-- <span class="right badge badge-danger">New</span> --}}
        </p>
    </a>
</li>

{{--<li class="nav-item">--}}
{{--    <a href="{{ route('users', 'all') }}" class="nav-link @if(Route::current()->getName() == 'users') active @endif">--}}
{{--        <i class="nav-icon fas fa-users-cog"></i>--}}

{{--        <p>--}}
{{--            UTILISATEURS--}}
{{--            --}}{{-- <span class="right badge badge-danger">New</span> --}}
{{--        </p>--}}
{{--    </a>--}}
{{--</li>--}}
