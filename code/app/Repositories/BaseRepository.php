<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    /**
     * Model.
     *
     * @var Model
     */
    protected $model;

    /**
     * Store a new model.
     *
     * @param array $input
     * @return Model
     */
    public function  store(array $input){
        return $this->model->create($input);
    }
    public function getBySlug($slug)
    {
        return $this->model->whereSlug($slug)->firstOrFail();
    }
    /**
     * Get model by user.
     *
     * @param  string
     * @return Model $model
     */
    public function getByUser($id)
    {
        return $this->model->whereUserId($id)->get();
    }



    /**
     * Get all.
     *
     * @param string
     * @return Collection|Model[]
     */
    public function getAll()
    {
        return $this->model->all();
    }
}
