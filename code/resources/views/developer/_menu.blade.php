<li class="nav-item">
    <a href="{{ route('apps.index') }}" class="nav-link @if(Route::current()->getName() == 'developer') active @endif">
        <i class="nav-icon fas fa-th"></i>
        <p>
            ACCUEIL
            {{-- <span class="right badge badge-danger">New</span> --}}
        </p>
    </a>
     <a href="{{ route('etablissement') }}" class="nav-link @if(Route::current()->getName() == 'etablissement') active @endif">
        <i class="nav-icon fas fa-hospital"></i>
        <p>
            Etablissements
            {{-- <span class="right badge badge-danger">New</span> --}}
        </p>
    </a>
    <a href="{{ route('medicament') }}" class="nav-link @if(Route::current()->getName() == 'medicament') active @endif">
        <i class="nav-icon fas fa-pills"></i>
        <p>
            Medicament
            {{-- <span class="right badge badge-danger">New</span> --}}
        </p>
    </a>
    <a href="{{ route('stock') }}" class="nav-link @if(Route::current()->getName() == 'stock') active @endif">
        <i class="nav-icon fas fa-store"></i>
        <p>
            Stock
            {{-- <span class="right badge badge-danger">New</span> --}}
        </p>
    </a>
    <a href="{{ route('operations') }}" class="nav-link @if(Route::current()->getName() == 'operations') active @endif">
        <i class="nav-icon fas fa-cash-register"></i>
        <p>
            Operation
            {{-- <span class="right badge badge-danger">New</span> --}}
        </p>
    </a>
    
</li>
