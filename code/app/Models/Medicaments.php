<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Medicaments extends Model
{

    public $incrementing = false;

    protected $fillable = [
         'id','name','description'
    ];

    public  function stocks(){
       return $this->hasMany(Stock::class);
    }
}

