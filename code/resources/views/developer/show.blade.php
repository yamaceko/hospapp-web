@extends('apps.layout')

@section('title')
{{ $user->name }} | Fiche du utilisateur
@endsection
@section('breadcrumb-title')
{{ $user->name }}
@endsection

@section('aside-menu')
@include('developer/_menu')
@endsection
@section('custom-style')
<style>
    .btn-circle.btn-xl {
        width: 70px;
        height: 70px;
        padding: 10px 16px;
        border-radius: 35px;
        font-size: 24px;
        line-height: 1.33;
    }

    .btn-circle {
        width: 30px;
        height: 30px;
        padding: 6px 0px;
        border-radius: 15px;
        text-align: center;
        font-size: 12px;
        line-height: 1.42857;
    }

</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        {{-- Gestion des erreurs --}}
        <div class="col-lg-12">
            @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                @if ($errors->has('message'))
                <div class="help-block">
                    <strong>{{ $errors->first('message') }}</strong>
                </div>
                @endif



                @if ($errors->has('name'))
                <div class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </div>
                @endif

                @if ($errors->has('number_phone'))
                <div class="help-block">
                    <strong>{{ $errors->first('number_phone') }}</strong>
                </div>
                @endif

                @if ($errors->has('email'))
                <div class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
                @endif

                @if ($errors->has('password'))
                <div class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </div>
                @endif

                @if ($errors->has('roles'))
                <div class="help-block">
                    <strong>{{ $errors->first('roles') }}</strong>
                </div>
                @endif
            </div>
            @endif

            @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">*
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                {{ session()->get('message') }}
            </div>
            @endif
        </div>
        {{-- Information sur l'utilisateur --}}
        <div class="col-lg-12">
            <div class="card card-dark">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h3 class="card-title">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Info ustilisateur: {{$user->name}}</font>
                                </font>
                                @if($user->roles)
                                <span class="badge badge-primary"> {{$user->roles->name}}</span>

                                @else
                                    <span class="badge badge-danger">...</span>
                                @endif
                            </h3>


                        </div>

                        <div class="right">
                            <button type="submit" class="btn btn-warning " onclick="myFunction()">
                                <font style="vertical-align: inherit; color:white; font-weigth:bold;">
                                    <font style="vertical-align: inherit;">Modifier</font>
                                </font>
                            </button>

                        </div>
                    </div>

                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Nom</font>
                                </font>
                            </label>
                            <input type="text" class="form-control" id="userName" placeholder="Nom "
                                value="{{$user->name}} " disabled>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Adresse électronique</font>
                                </font>
                            </label>
                            <input type="email" class="form-control" id="userMail" placeholder="Enter email"
                                value="{{$user->email}}" disabled>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">
                                <span style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Numero de telephone</font>
                                </span>
                            </label>
                            <input type="number" class="form-control" id="userNumber" placeholder="Enter email"
                                value="{{$user->number_phone}}" disabled>
                        </div>
                        <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
                            <label for="role" class="col-md-4 control-label">Rôle</label>

                            <select id="role_id" name="role" class="form-control">
                                @foreach($roles as $role)
                                <option selected="{{$user->role}}" value="{{ $role->id }}" disabled>{{ $role->name }}
                                </option>
                                @endforeach
                            </select>

                        </div>

                        <div class="row">

                            <div class="col">

                                @if($user->etablissements)
                                <span class="badge badge-dark">Etablissements:
                                    @foreach($user->etablissements as $etablissement)
                                    <span class="badge badge-warning">
                                        @if( !is_null($etablissement->name))
                                        {{$etablissement->name }}
                                        @else
                                        <h3 class="badge badge-warning">aucun etablissement</h3>
                                        @endif
                                    </span>
                                    @endforeach
                                    <a href="#" title="ajouter un etablissement"
                                        class="btn btn-outline-success btn-sm ml-2" data-toggle="modal"
                                        data-target="#modalRolesID{{$user->id}}"><i class="fas fa-plus"
                                            disabled></i></a>

                                </span>

                                @else
                                <span class="badge badge-danger">l'utilisateur n'a aucun exif_thumbnail</span>
                                @endif
                            </div>



                            
                        </div>


                    </div>
                    <!-- /.card-body -->
                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Soumettre</font>
                            </font>
                        </button>
                    </div>
                </form>
                <div class="modal fade text-left" id="modalRolesID{{$user->id}}" tabindex="-1" role="dialog"
                                aria-labelledby="modalRolesID{{$user->id}}" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <form class="modal-content form-horizontal" method="POST" action="#">
                                        {{ csrf_field() }}

                                        <div class="modal-header text-center">
                                            <h4 class="modal-title w-100 font-weight-bold">Mise à jour des informations
                                                utilisateur - ajouts d'établissement</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body">
                                            <!-- User roles -->
                                            <div class="col-lg-12 text-center">
                                                <span>
                                                    etablissements affetés:
                                                     @foreach ($user->etablissements as $etablissement)
                                                <h4 class="badge badge-warning" style="font-size:1rem;">
                                                    {{ $etablissement->name }}</h4>
                                                @endforeach
                                                </span>
                                               
                                            </div>

                                            <!-- Roles -->
                                            <div class="form-group{{ $errors->has('etablissements') ? ' has-error' : '' }}">
                                                <label for="roles" class="col-md-4 control-label">Etablissements:</label>

                                                <div class="col-lg-12">
                                                    <div class="input-group mb-3">
                                                        <select multiple="multiple" name="etablissements[]" id="roles"
                                                            class="form-control">
                                                            @foreach($etablissements as $etablissement)
                                                               
                                                                <option value="{{$etablissement->id}}">{{$etablissement->name}}
                                                            </option>
                            
                                                            @endforeach
                                                        </select>

                                                        <div class="input-group-append input-group-text">
                                                            <span class="fas fa-building"></span>
                                                        </div>
                                                    </div>

                                                    @if ($errors->has('etablissements'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('etablissements') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-default">Modifier les informations de
                                                l'utilisateur</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    function myFunction() {
        document.getElementById("userName").disabled = false;
        document.getElementById("userNumber").disabled = false;
        console.log('====================================');
        console.log('tester ');
        console.log('====================================');
    }

</script>
@endsection
